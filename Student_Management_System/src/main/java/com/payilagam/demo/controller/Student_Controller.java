package com.payilagam.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.demo.entity.Student_Entity;
import com.payilagam.demo.repository.Student_Repository;

@Controller
public class Student_Controller 
{
	@Autowired
	Student_Repository repo;//global variable we have to give the repository name and reference
	
	@RequestMapping("login")
	public ModelAndView display()
	{
		ModelAndView mv = new ModelAndView("student");
		mv.addObject("stud",repo.findAll());
		return mv;
	}
	
	@RequestMapping("student/new")
	public ModelAndView create(Student_Entity entity) {
		
		ModelAndView mv = new ModelAndView("createstatement");
		mv.addObject("empty_entity", entity);
		return mv;
	}
	
	@PostMapping("new_student")
	public String savestudents(Student_Entity stud) {
		repo.save(stud);
		
		return "redirect:/login";
		}
	
	@RequestMapping("/edit/{Id}")
	public ModelAndView editStudent(@PathVariable("Id") int id) {
		ModelAndView mv = new ModelAndView("editstudent");
		Student_Entity entity=repo.findById(id).get();
		mv.addObject("edit", entity);
		System.out.println("updating===============================");
	 return mv;	
		
	}
	
	@PostMapping("/student/edit/{Id}")
		public String updateStudent(Student_Entity entity) {
			repo.save(entity);
			return "redirect:/login";
		}
	
	@RequestMapping("/delete/{Id}")
	public String deleteStudent(@PathVariable("Id") int id) {
		repo.deleteById(id);
		return "redirect:/login";
	}
}
