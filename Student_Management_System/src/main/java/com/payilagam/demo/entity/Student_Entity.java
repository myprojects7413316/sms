package com.payilagam.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity    
//It is a pojo class
@Table(name="Student_Details")
public class Student_Entity 
{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int student_id;
	@Column(name="Name")
	private String name;
	private String student_last_name;
	private String student_email;
	
	
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	
	public String getStudent_last_name() {
		return student_last_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setStudent_last_name(String student_last_name) {
		this.student_last_name = student_last_name;
	}
	public String getStudent_email() {
		return student_email;
	}
	public void setStudent_email(String student_email) {
		this.student_email = student_email;
	}
	
}
