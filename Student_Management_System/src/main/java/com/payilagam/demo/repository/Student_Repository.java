package com.payilagam.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.demo.entity.Student_Entity;

public interface Student_Repository extends JpaRepository<Student_Entity,Integer>
{

}
